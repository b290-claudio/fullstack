// import {Row, Col, Card, Button} from 'react-bootstrap';

/*export default function CourseCard() {
	return (

		<Row className="mt-3 mb-3">
			<Col xs={12}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							Sample Course
						</Card.Title>
						<Card.Text>
							Description: <br />
							This is a sample course offering. <br />
							<br/>
							Price:<br />
							PhP 40,000 <br />
						</Card.Text>
						<Button variant="primary">Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}*/


/*
	^^^^^^^^^^^^^THIS IS ACTIVITY S50^^^^^^^^^^^^^
	1. Create CourseCard component with Course Name, Description and price
		a. course name in card title
		b. description and price in card body

	2. Render the CourseCard component in the Home page
*/

/*===============================================================*/

/*import { Card, Button } from 'react-bootstrap';


export default function CourseCard(props) {

	// Checks to see if the data was successfully passed
	console.log(props);
	// Every component receives information in a form of an object
	console.log(typeof props);
    return (
        <Card>
            <Card.Body>
                <Card.Title>{props.course.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{props.course.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {props.course.price}</Card.Text>
                <Button variant="primary">Enroll</Button>
            </Card.Body>
        </Card>
    )
}*/


/*===============================================================*/
/*=========================Destructuring=========================*/

/*import { Card, Button } from 'react-bootstrap';


export default function CourseCard({course}) {

	// Checks to see if the data was successfully passed
	console.log(course);
	// Every component receives information in a form of an object
	console.log(typeof course);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{course.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {course.price}</Card.Text>
                <Button variant="primary">Enroll</Button>
            </Card.Body>
        </Card>
    )
}*/


/*===============================================================*/
/*=========================Destructuring=========================*/
import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function CourseCard({course}) {

	// Checks to see if the data was successfully passed
	console.log(course);
	// Every component receives information in a form of an object
	console.log(typeof course);

	const { _id, name, description, price } = course;


	// Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax
        // const [getter, setter] = useState(initialGetterValue);
	// const [ count, setCount ] = useState(0);
	// Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

	// console.log(useState(0));


/*==========================ACTIVITY S51==========================*/

	/*const [ seats, setSeats ] = useState(30);

	function enroll(){
		if((count < 30) && (seats !== 0)){
			setCount(count + 1);
			setSeats(seats - 1);
		} else {
			alert(`No more seats.`)
		}
	}*/
/*======================End of ACTIVITY S51======================*/


	// const [ seats, setSeats ] = useState(30);

	// const [ isOpen, setIsOpen ] = useState(true);

	// function enroll(){
	// 	setCount(count + 1);
	// 	setSeats(seats - 1);
	// }

	// useEffect(() => {
	// 	if(seats === 0){
	// 		// setSeats(false);
	// 		// alert(`No more seats available`)
	// 		setIsOpen(false);
	// 	}
	// }, [seats])


    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}