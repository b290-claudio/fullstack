let collection = [];

let i = 0;
function print(){
	return collection;
}

function enqueue(a){
	for(i = 0; i<collection.length; i++){
		if(collection[i]==undefined){
			collection[i] = collection[i+1]
		}
	}
	collection[i] = a;
	i++;
	return collection;
}

function dequeue(b){
	for(let j = 1; j<collection.length; j++){
		collection[j-1] = collection[j]
	}
	collection.length--;
	return collection;
}

function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	let checkEmpty;
	if(collection.length>0){
		return false;
	} else {
		return true
	}
}



module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};