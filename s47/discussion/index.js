// alert("Hello Batch 290!");

// querySelector() is a method that can be used to select a specific object/element from our document.
console.log(document.querySelector("#txt-first-name"));

// document refers to the whole page
console.log(document)

/*
	Alternative ways to access HTML elements. This is what we can use aside from the querySelector().

	document.getElementById("txt-first-name");
	document.getElementsByClassName("txt-first-name");
	document.getElementsByTagName("input");
*/

console.log(document.getElementById("txt-first-name"));

// -------------EVENT & EVENT LISTENERS --------------

const textFirstName = document.querySelector("#txt-first-name");
const textLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

console.log(textFirstName);
console.log(spanFullName);

/*

	Event
		ex: click, hover, keypress and many other events

	Event Listeners
		Allows us to let our user/s interact with our page. With each click or hover there is an event which triggers a function/task.

	Syntax:
		selectedElement.addEventListener("event", function);

*/

	// "innerHTML" property retrieves the HTML content/children within the element
	// "value" property retrieves the value from the HTML element
textFirstName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = textFirstName.value + " " + textLastName.value
})

textLastName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = textFirstName.value + " " + textLastName.value
})


// Alternative way to write the code for event handling
textLastName.addEventListener("keyup", printLastName);

function printLastName(event){
	spanFullName.innerHTML = textLastName.value
}

textFirstName.addEventListener("keyup", event => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})

/*
	The "event" argument contains the information on the triggered event.
	The "event.target" contains the element where the event happened.
	the "event.target.value" gets the value of the input object (this is similar to txtFirstName.value).
*/

/* Mini Activity */
/*
	Create an event listener that when first name label is clicked an alert message "You clicked First Name label" will prompt
*/

const labelFirstName = document.querySelector("#label-first-name");

labelFirstName.addEventListener("click", event => {
	alert("You clicked First Name label");
	console.log(event);
	console.log(event.target);
});