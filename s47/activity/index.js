const textFirstName = document.querySelector("#txt-first-name");
const textLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


textFirstName.addEventListener("keyup", printFullName);
textLastName.addEventListener("keyup", printFullName);

function printFullName(event){
	spanFullName.innerHTML = textFirstName.value + " " + textLastName.value
}